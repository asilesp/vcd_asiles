write-host "############################################"
write-host "######### INFO DE TODOS LOS TENANT #########"
write-host "############################################"

$edgesvcd = (Get-NSXEdge | where {$_.name -like "vse*"})

Foreach ($i in $edgesvcd)
{

$uplinks = ($i.vnics.vnic | where {$_.type -like "*uplink*"})

$privateip = ($uplinks.addressGroups.addressGroup | where {$_.subnetPrefixLength -like "*28*"}).primaryAddress
$publicip = ($uplinks.addressGroups.addressGroup | where {$_.subnetPrefixLength -notlike "*28*"}).primaryAddress


$edge1 = Get-NsxEdge -Name EDGE-VCD-EDG-01
$routing1 = Get-NsxEdgeRouting -Edge $edge1
$asn = (Get-NsxEdgeBgpNeighbour -EdgeRouting $routing1 | where {$_.ipAddress -like "*$privateip*"} ).remoteASNumber


$name = $i.name
$pg = $uplinks.portgroupName

$vdnId = $pg.Split('-')[6]

$logicalswitch = (Get-NsxLogicalSwitch | where {$_.vdnId -like "*$vdnId*"}).name

$externalname = (Get-ExternalNetwork | where {$_.name -like "*$vdnId*"}).name
$externalgw = (Get-ExternalNetwork | where {$_.name -like "*$vdnId*"}).Gateway.IPAddressToString

$tenant = $i.tenant

$org = Get-Org | where {$_.Href -like "*$tenant*"}

write-host " "
write-host "Organization: $org"
write-host "EDGE: $name"
write-host "PortGroup: $pg"
write-host "VXLAN ID: $vdnId"
write-host "Logical Switch: $logicalswitch"
write-host "External Network Name: $externalname"
write-host "External Network Gateway: $externalgw"
write-host "Private IP: $privateip"
write-host "Public IP: $publicip"
write-host "ASN: $asn "
write-host " "

}