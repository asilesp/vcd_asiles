write-host "####################################"
write-host "######### AÑADIR UN TENANT #########"
write-host "####################################"

######### COSAS QUE TENDRIAN QUE DARME #########


#$name = "Ext-Net-TransitECMP-03"
$name = Read-host "Logical Switch Name"
#$transitGateway = "100.88.43.225"
$transitGateway = Read-host "Gateway"
#$transitNetmask = "255.255.255.240"
$transitNetmask = Read-host "Netmask"
#$asn = 65213
$asn = Read-host "ASN"

#$transitStart = "100.88.43.226"
#$transitEnd = "100.88.43.238"

#$edgeaddress = "100.88.43.229"

######### IPS #########

$0 = $transitGateway.Split('.')[0]
$1 = $transitGateway.Split('.')[1]
$2 = $transitGateway.Split('.')[2]
$3 = $transitGateway.Split('.')[3]

$aux = 1
$aux2 = 13

$3transitstart = [int]$3+[int]$aux
$3transitend = [int]$3+[int]$aux2


$3edge1 = $3
$3edge2 = [int]$3+[int]$aux
$3edge3 = [int]$3edge2+[int]$aux
$3edge4 = [int]$3edge3+[int]$aux
$3edgetenant = [int]$3edge4+[int]$aux


$transitStart = "$0.$1.$2.$3transitstart"
$transitEnd = "$0.$1.$2.$3transitend"

$edge1address = "$0.$1.$2.$3edge1"
$edge2address = "$0.$1.$2.$3edge2"
$edge3address = "$0.$1.$2.$3edge3"
$edge4address = "$0.$1.$2.$3edge4"

$edgetenantaddress = "$0.$1.$2.$3edgetenant" 

write-host "####IPS####"
write-host "Gateway $transitGateway"
write-host "Netmask $transitNetmask"
write-host "Start: $transitStart"
write-host "End: $transitEnd"
write-host "..."
write-host "Edge 1: $edge1address"
write-host "Edge 2: $edge2address"
write-host "Edge 3: $edge3address"
write-host "Edge 4: $edge4address"
write-host "..."
write-host "Edge Tenant: $edgetenantaddress"
write-host "Edge ASN: $asn"

######### CREAR LOGICAL SIWTTCH ######### 
write-host "Creating Logical Switch ...."

$TZ = Get-NsxTransportZone -name "VCD-Transport-Zone"

New-NsxLogicalSwitch -TransportZone $TZ -Name $name -ControlPlaneMode UNICAST_MODE -TenantId "virtual wire tenant"


$vdnId = (Get-NsxLogicalSwitch -Name $name | select vdnId).vdnId #VXLAN Id
$pg = (Get-VirtualPortGroup | where {$_.name -like "*21*$vdnId*"}).name #portgroup

$ls = Get-NsxLogicalSwitch -Name $name

$external = "$name-VXLAN$vdnId"


write-host "Logical Switch $name Created!"
######### CREAR EXTERNAL NETWORK #########
write-host "Creating External Network...."


$dvPG = $pg
$vCSName = "vcsa-vcd-edg-01.fabricmgmt.mdcloud.cat"

$vcloud = $DefaultCIServers[0].ExtensionData
$admin = $vcloud.GetAdmin()
$ext = $admin.GetExtension()

$mynetwork = new-object vmware.vimautomation.cloud.views.VMWExternalNetwork
$mynetwork.Name = $external
$mynetwork.Description = " "

$vCenter = Search-Cloud VirtualCenter | Get-CIView | where {$_.name -eq $vCSName}
$dvpg = get-view -viewtype DistributedVirtualPortGroup | where {$_.name -like $dvPG}



$mynetwork.VimPortGroupRef = new-object VMware.VimAutomation.Cloud.Views.VimObjectRef

$mynetwork.VimPortGroupRef.MoRef = $dvPG.key
#$mynetwork.VimPortGroupRef.VimObjectType = "NETWORK"
$mynetwork.VimPortGroupRef.VimObjectType = "DV_PORTGROUP"

$mynetwork.VimPortGroupRef.VimServerRef = new-object VMware.VimAutomation.Cloud.Views.Reference
$mynetwork.VimPortGroupRef.VimServerRef.href = $vCenter.href
#$mynetwork.VimPortGroupRef.VimServerRef.type = "application/vnd.vmware.admin.vmwvirtualcenter+xml"

$mynetwork.Configuration = new-object VMware.VimAutomation.Cloud.Views.NetworkConfiguration
$mynetwork.configuration.fencemode = "isolated"

$mynetwork.Configuration.IpScopes = new-object VMware.VimAutomation.Cloud.Views.IpScopes
$mynetwork.Configuration.IpScopes.IpScope = new-object VMware.VimAutomation.Cloud.Views.IpScope
$mynetwork.Configuration.IpScopes.ipscope[0].Gateway = $transitGateway
$mynetwork.Configuration.IpScopes.ipscope[0].Netmask = $transitNetmask
$mynetwork.Configuration.IpScopes.ipscope[0].IsInherited = "False"
$mynetwork.Configuration.IpScopes.ipscope[0].ipranges = new-object vmware.vimautomation.cloud.views.ipranges
$mynetwork.Configuration.Ipscopes.ipscope[0].ipranges.iprange = new-object vmware.vimautomation.cloud.views.iprange
$mynetwork.Configuration.IpScopes.ipscope[0].IpRanges.IpRange[0].startaddress = $transitStart
$mynetwork.Configuration.IpScopes.ipscope[0].IpRanges.IpRange[0].endaddress = $transitEnd


$result = $ext.CreateExternalNet($mynetwork)

$result


write-host "External Network $external created!"
########EDGE 1###########
write-host "Configuring EDGE 1"


$edge1 = Get-NsxEdge -Name EDGE-VCD-EDG-01
$interface = Get-NsxEdgeInterface -Edge $edge1 -Name Edge-Tenant-Trunks

$ids = Get-NsxEdgeSubInterface -Interface $interface | select tunnelId
$max =  ($ids.tunnelId | measure -Maximum).Maximum
$new = $max+1


New-NsxEdgeSubInterface -Interface $interface -TunnelId $new -Network $ls -Name $name -PrimaryAddress $edge1address -SubnetPrefixLength 28

write-host "Configured!"
write-host "Configuring BGP..."


$edge1 = Get-NsxEdge -Name EDGE-VCD-EDG-01
$routing1 = Get-NsxEdgeRouting -Edge $edge1
New-NsxEdgeBgpNeighbour -EdgeRouting $routing1 -IpAddress $edgetenantaddress -RemoteAS $asn
write-host "Configured!"

########EDGE 2###########
write-host "Configuring EDGE 2"


$edge2 = Get-NsxEdge -Name EDGE-VCD-EDG-02
$interface = Get-NsxEdgeInterface -Edge $edge2 -Name Edge-Tenant-Trunks

$ids = Get-NsxEdgeSubInterface -Interface $interface | select tunnelId
$max =  ($ids.tunnelId | measure -Maximum).Maximum
$new = $max+1


New-NsxEdgeSubInterface -Interface $interface -TunnelId $new -Network $ls -Name $name -PrimaryAddress $edge2address -SubnetPrefixLength 28

write-host "Configured!"

write-host "Configuring BGP..."

$edge2 = Get-NsxEdge -Name EDGE-VCD-EDG-02
$routing2 = Get-NsxEdgeRouting -Edge $edge2
New-NsxEdgeBgpNeighbour -EdgeRouting $routing2 -IpAddress $edgetenantaddress -RemoteAS $asn
write-host "Configured!"


########EDGE 3###########
write-host "Configuring EDGE 3"


$edge3 = Get-NsxEdge -Name EDGE-VCD-EDG-03
$interface = Get-NsxEdgeInterface -Edge $edge3 -Name Edge-Tenant-Trunks

$ids = Get-NsxEdgeSubInterface -Interface $interface | select tunnelId
$max =  ($ids.tunnelId | measure -Maximum).Maximum
$new = $max+1


New-NsxEdgeSubInterface -Interface $interface -TunnelId $new -Network $ls -Name $name -PrimaryAddress $edge3address -SubnetPrefixLength 28

write-host "Configured!"

write-host "Configuring BGP..."

$edge3 = Get-NsxEdge -Name EDGE-VCD-EDG-03
$routing3 = Get-NsxEdgeRouting -Edge $edge3
New-NsxEdgeBgpNeighbour -EdgeRouting $routing3 -IpAddress $edgetenantaddress -RemoteAS $asn
write-host "Configured!"

########EDGE 4###########
write-host "Configuring EDGE 4"


$edge4 = Get-NsxEdge -Name EDGE-VCD-EDG-04
$interface = Get-NsxEdgeInterface -Edge $edge4 -Name Edge-Tenant-Trunks

$ids = Get-NsxEdgeSubInterface -Interface $interface | select tunnelId
$max =  ($ids.tunnelId | measure -Maximum).Maximum
$new = $max+1


New-NsxEdgeSubInterface -Interface $interface -TunnelId $new -Network $ls -Name $name -PrimaryAddress $edge4address -SubnetPrefixLength 28

write-host "Configured!"

write-host "Configuring BGP..."
$edge4 = Get-NsxEdge -Name EDGE-VCD-EDG-04
$routing4 = Get-NsxEdgeRouting -Edge $edge4
New-NsxEdgeBgpNeighbour -EdgeRouting $routing4 -IpAddress $edgetenantaddress -RemoteAS $asn
write-host "Configured!"



####### CREATE ORG-VDC #####

#https://kb.vmware.com/s/article/2147538 :(

#$PVDC = Get-ProviderVdc  -Name "PVDC-Mediacloud-Zona1"
#$netpool = Get-NetworkPool -Name "VCD-Transport-Zone"