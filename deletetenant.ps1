write-host "######################################"
write-host "######### ELIMINAR UN TENANT #########"
write-host "######################################"


$logicalswitch = Read-Host "LogicalSwitch Name:"
$asn = Read-Host "ASN:"


$nombre = (Get-NsxLogicalSwitch -Name $logicalswitch).Name
$vdnId = (Get-NsxLogicalSwitch -Name $logicalswitch).vdnId

$pg = (Get-VirtualPortGroup | where {$_.name -like "*21*$vdnId*"}).name #portgroup


#external network

$externalname = (Get-ExternalNetwork | where {$_.name -like "*$vdnId*"}).name
$externalgw = (Get-ExternalNetwork | where {$_.name -like "*$vdnId*"}).Gateway



#### Quitar interfaces de los edges

#Edge 1

write-host "EDGE 1"
$edge1 = Get-NsxEdge -Name EDGE-VCD-EDG-01
$routing1 = Get-NsxEdgeRouting -Edge $edge1
Get-NsxEdgeBgpNeighbour -EdgeRouting $routing1 -RemoteAS $asn | Remove-NsxEdgeBgpNeighbour

$edge1 = Get-NsxEdge -Name EDGE-VCD-EDG-01
$interface = Get-NsxEdgeInterface -Edge $edge1 -Name Edge-Tenant-Trunks
Get-NsxEdgeSubInterface -Interface $interface -name $logicalswitch | Remove-NsxEdgeSubInterface 

#Edge 2
write-host "EDGE 2"
$edge2 = Get-NsxEdge -Name EDGE-VCD-EDG-02
$routing2 = Get-NsxEdgeRouting -Edge $edge2
Get-NsxEdgeBgpNeighbour -EdgeRouting $routing2 -RemoteAS $asn | Remove-NsxEdgeBgpNeighbour

$edge2 = Get-NsxEdge -Name EDGE-VCD-EDG-02
$interface = Get-NsxEdgeInterface -Edge $edge2 -Name Edge-Tenant-Trunks
Get-NsxEdgeSubInterface -Interface $interface -name $logicalswitch | Remove-NsxEdgeSubInterface 

#Edge 3
write-host "EDGE 3"
$edge3 = Get-NsxEdge -Name EDGE-VCD-EDG-03
$routing3 = Get-NsxEdgeRouting -Edge $edge3
Get-NsxEdgeBgpNeighbour -EdgeRouting $routing3 -RemoteAS $asn | Remove-NsxEdgeBgpNeighbour

$edge3 = Get-NsxEdge -Name EDGE-VCD-EDG-03
$interface = Get-NsxEdgeInterface -Edge $edge3 -Name Edge-Tenant-Trunks
Get-NsxEdgeSubInterface -Interface $interface -name $logicalswitch | Remove-NsxEdgeSubInterface 

#Edge 4
write-host "EDGE 4"
$edge4 = Get-NsxEdge -Name EDGE-VCD-EDG-04
$routing4 = Get-NsxEdgeRouting -Edge $edge4
Get-NsxEdgeBgpNeighbour -EdgeRouting $routing4 -RemoteAS $asn | Remove-NsxEdgeBgpNeighbour

$edge4 = Get-NsxEdge -Name EDGE-VCD-EDG-04
$interface = Get-NsxEdgeInterface -Edge $edge4 -Name Edge-Tenant-Trunks
Get-NsxEdgeSubInterface -Interface $interface -name $logicalswitch | Remove-NsxEdgeSubInterface 

#### Eliminar External Networkc