write-host "#####################################"
write-host "######### INFO DE UN TENANT #########"
write-host "#####################################"

#nombre del Logical Switch

$logicalswitch = Read-Host "LogicalSwitch Name:"
$asn = Read-Host "ASN:"

$nombre = (Get-NsxLogicalSwitch -Name $logicalswitch).Name
$vdnId = (Get-NsxLogicalSwitch -Name $logicalswitch).vdnId

$pg = (Get-VirtualPortGroup | where {$_.name -like "*21*$vdnId*"}).name #portgroup


#external network

$externalname = (Get-ExternalNetwork | where {$_.name -like "*$vdnId*"}).name
$externalgw = (Get-ExternalNetwork | where {$_.name -like "*$vdnId*"}).Gateway


#edge1

$edge1 = Get-NsxEdge -Name EDGE-VCD-EDG-01
$interface = Get-NsxEdgeInterface -Edge $edge1 -Name Edge-Tenant-Trunks
$edge1name = (Get-NsxEdgeSubInterface -Interface $interface -name $logicalswitch).name
$edge1tunnel = (Get-NsxEdgeSubInterface -Interface $interface -name $logicalswitch).tunnelId
$edge1ip = (Get-NsxEdgeSubInterface -Interface $interface -name $logicalswitch).addressGroups.addressGroup.primaryAddress

$routing1 = Get-NsxEdgeRouting -Edge $edge1
$neiip1= (Get-NsxEdgeBgpNeighbour -EdgeRouting $routing1 -RemoteAS $asn).ipAddress

#edge2

$edge2 = Get-NsxEdge -Name EDGE-VCD-EDG-02
$interface = Get-NsxEdgeInterface -Edge $edge2 -Name Edge-Tenant-Trunks
$edge2name = (Get-NsxEdgeSubInterface -Interface $interface -name $logicalswitch).name
$edge2tunnel = (Get-NsxEdgeSubInterface -Interface $interface -name $logicalswitch).tunnelId
$edge2ip = (Get-NsxEdgeSubInterface -Interface $interface -name $logicalswitch).addressGroups.addressGroup.primaryAddress

$routing2 = Get-NsxEdgeRouting -Edge $edge2
$neiip2= (Get-NsxEdgeBgpNeighbour -EdgeRouting $routing2 -RemoteAS $asn).ipAddress

#edge3

$edge3 = Get-NsxEdge -Name EDGE-VCD-EDG-03
$interface = Get-NsxEdgeInterface -Edge $edge3 -Name Edge-Tenant-Trunks
$edge3name = (Get-NsxEdgeSubInterface -Interface $interface -name $logicalswitch).name
$edge3tunnel = (Get-NsxEdgeSubInterface -Interface $interface -name $logicalswitch).tunnelId
$edge3ip = (Get-NsxEdgeSubInterface -Interface $interface -name $logicalswitch).addressGroups.addressGroup.primaryAddress

$routing3 = Get-NsxEdgeRouting -Edge $edge3
$neiip3= (Get-NsxEdgeBgpNeighbour -EdgeRouting $routing3 -RemoteAS $asn).ipAddress

#edge4

$edge4 = Get-NsxEdge -Name EDGE-VCD-EDG-04
$interface = Get-NsxEdgeInterface -Edge $edge4 -Name Edge-Tenant-Trunks
$edge4name = (Get-NsxEdgeSubInterface -Interface $interface -name $logicalswitch).name
$edge4tunnel = (Get-NsxEdgeSubInterface -Interface $interface -name $logicalswitch).tunnelId
$edge4ip = (Get-NsxEdgeSubInterface -Interface $interface -name $logicalswitch).addressGroups.addressGroup.primaryAddress

$routing4 = Get-NsxEdgeRouting -Edge $edge4
$neiip4= (Get-NsxEdgeBgpNeighbour -EdgeRouting $routing4 -RemoteAS $asn).ipAddress

write-host "###############"
write-host "LOGICAL SWITCH:"
write-host "Nombre: $nombre"
write-host "VXLAN: $vdnId"
write-host "PortGroup: $pg"
write-host " "
write-host "###############"
write-host "EXTERNAL NETWORK:"
write-host "Nombre: $externalname"
write-host "Gateway: $externalgw"
write-host " "
write-host "###############"
write-host "ECMP EDGES:"
write-host " "
write-host "EDGE1:"
write-host "Interface: $edge1name"
write-host "tunnelId: $edge1tunnel"
write-host "IP: $edge1ip"
write-host "Neighbour IP: $neiip1"
write-host " "
write-host "EDGE2:"
write-host "Interface: $edge2name"
write-host "tunnelId: $edge2tunnel"
write-host "IP: $edge2ip"
write-host "Neighbour IP: $neiip2"
write-host " "
write-host "EDGE3:"
write-host "Interface: $edge3name"
write-host "tunnelId: $edge3tunnel"
write-host "IP: $edge3ip"
write-host "Neighbour IP: $neiip3"
write-host " "
write-host "EDGE4:"
write-host "Interface: $edge4name"
write-host "tunnelId: $edge4tunnel"
write-host "IP: $edge4ip"
write-host "Neighbour IP: $neiip4"
write-host " "